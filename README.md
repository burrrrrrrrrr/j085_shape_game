# j085_shape_game

## Description
* Some sort of multiplayer board game 
* Early in development 
* Players can capture squares adjacent to ones they have already captured

## Setup
* npm install 
* npm run dev 
* visit localhost:3000/game
