import React, {useEffect, useContext} from "react";
import GameContext from '../../context/game/gameContext'
import io from "socket.io-client";

export const Players = () => {
    const gameContext = useContext(GameContext)

    const {game, getGame, setGame} = gameContext

    useEffect(() => {
        // getGame()
        props.socket.on('getPlayers', function(serverSquares){
            setGame(serverSquares)
        });
        // es-lint-disable-next-line
    }, [])
   

    return (
        <div>
            
        </div>
    )
}

export default Players