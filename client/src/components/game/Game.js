import React, {useEffect, useContext} from "react";
import GameContext from '../../context/game/gameContext'
import io from "socket.io-client";
import Scores from '../score/Score'
  
export const Game = props => {
    const gameContext = useContext(GameContext)
    
    const {game, getGame, setGame, setCountdown, countdown} = gameContext

    useEffect(() => {
        // getGame()
        props.socket.on('setSquareClient', function(res){
            setGame(res.squares)
        });
        props.socket.on('setCountdown', function(res){
            setCountdown(true)
            setTimeout(() => {
                setCountdown(false)
            }, 1000);
        });
        // es-lint-disable-next-line
    }, [])

    const setSquare = e => {
        props.socket.emit('setSquare', {row: e.target.dataset.row, col: e.target.dataset.col});
    }

    let squares = []

    for (let i = 0; i < game.length; i++) {
        for (let j = 0; j < game.length; j++) {
            squares.push(
                <div
                    onClick={setSquare}
                    className="board-square"
                    data-row={i}
                    data-col={j}
                    style={{backgroundColor: game.length && game[i][j] !== "" ? game[i][j] : '' }}
                >
                </div>
            )
        }
    }

    let styles = {}

    if(countdown) {
        styles = {
            width: "100%",
            transition: "1000ms width linear"
        }   
    } else {
        styles = {
            width: "0",
            transition: "none"
        }
    }

    return (
        <div>
            <div className="turn-progress">
                <div 
                    style={styles}
                    className="turn-progress-inner">
                </div>
            </div>
            <div className="board-wrapper">
                <div className="board">
                    {squares}
                </div>
            </div>
        </div>
    )
}

export default Game