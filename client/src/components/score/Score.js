
import React, {useEffect, useContext} from 'react'
import ScoreContext from '../../context/score/scoreContext'

const Score = props => {
    const scoreContext = useContext(ScoreContext)
    
    const {setTimer, timer, setScores, scores, setGameOver, gameOver} = scoreContext
        
    useEffect(() => {
        props.socket.on('setScoreTable', function(res){
            setTimer(res.gameTimer)
        });

        props.socket.on('gameOver', function(res){
            setScores(res.scores)
            setGameOver(res.gameOver)
        });
        // es-lint-disable-next-line
    }, [])

    const scoreList = Object.keys(scores).map(key => 
        <article style={{width: "100%", display: "flex", alignItems: "center", order: String(1000 - scores[key].score)}} value={key}><div className="board-square" style={{backgroundColor: scores[key].color}}></div><span> {scores[key].score}</span></article>
    )

    return (
        <div className="scores">
           {gameOver === false ? "TIMER:" +  timer : scoreList }
            
            {/* {scores} */}
        </div>
    )
}

export default Score