import React, {useEffect, useContext} from "react";
// import Player from '../../components/player/Player';
// import PlayerState from '../../context/player/PlayerState'

// todo change this to 'register'

export const Player = props => {
    const colors = ["#801A00", "#FF0000", "#FF7519", "#FFFF00", "#008A00", "#0033CC", "#660099", "#FF80B5", "#A37ACC", "#FF00FF", "#000000", "#FFFFFF"];

    let playerName = null
    let playerColor = null

    useEffect(() => {
        // getGame()
        props.socket.on('registerPlayer', function(pl){
            // setGame(serverSquares)
        });
        // es-lint-disable-next-line
    }, [])

    const registerPlayer = e => {
        e.preventDefault();        
        props.socket.emit('registerPlayer', {name: playerName, color: playerColor});
        // props.socket.on('registerPlayer', function(serverSquares){
        //     setGame(serverSquares)
        // });
    }

    const handleNameChange = e => {
        playerName = e.target.value
    }

    const handleColorChange = e => {
        playerColor = e.target.style.backgroundColor
    }

    const colorsElems = colors.map((color) =>
        <div className="color" style={{backgroundColor: color}}><input name="color" data-color={{color}} required type="radio"  onClick={handleColorChange} /></div>
    )

    return (
        <form onSubmit={registerPlayer} className="player-wrapper">
            <div className="input-wrapper">
                <input type="text" onChange={handleNameChange} name="name" required placeholder="Enter your name..." />
            </div>
            <div className="color-wrapper">{colorsElems}</div>
            <div className="btn-wrapper">
                <button type="submit">Join Game</button>
            </div>
        </form>
    )
}

export default Player