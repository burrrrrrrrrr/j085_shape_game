
import React, {useEffect, useContext} from "react";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import io from "socket.io-client";
import Game from './components/game/Game';
import GameState from './context/game/GameState'
import Player from './components/player/Player';
import PlayerState from './context/player/PlayerState'
import Score from './components/score/Score';
import ScoreState from './context/score/ScoreState'

function App() {
    const location = window.location.host.split(':')[0] + ':5000'
    console.log(location)
    const socket = io()
    
    useEffect(() => {
      // es-lint-disable-next-line
    }, [])

  return (
    <div id="app" className="App">
      <PlayerState>
        <GameState>
          <ScoreState>
            <Router>
              {/* <Route 
                path="/" 
                render={(props) => <Player {...props} socket={socket} />} 
              /> */}
              <Score socket={socket} />
              <Route 
                path="/game" 
                render={(props) => <Game {...props} socket={socket} />} 
              />
            </Router>
          </ScoreState>
        </GameState>
      </PlayerState>
    </div>
  );
}

export default App;
