import React, { useReducer } from 'react'
import PlayerContext from './playerContext'
import PlayerReducer from './playerReducer'
import {
    SET_GAME,
    GET_GAME
} from '../types'

export const PlayerState = props => {
    let squares = [];
    for (let i = 0; i < 20; i++) {
        squares[i] = [];
        for (let j = 0; j < 20; j++) {
            squares[i][j] = '';
        }            
    }

    const initialState = {
        player: squares
    }

    const [state, dispatch] = useReducer(PlayerReducer, initialState)

    const getPlayer = () => {
        
    }

    const setPlayer = player => {
        dispatch({
            type: SET_GAME,
            payload: player 
        })
    }

    return (
        <PlayerContext.Provider
            value={{
                player: state.player,
                getPlayer,
                setPlayer
            }}
        >
            {props.children}
        </PlayerContext.Provider>
    )
}

export default PlayerState
