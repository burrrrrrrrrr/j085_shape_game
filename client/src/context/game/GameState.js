import React, { useReducer } from 'react'
import GameContext from './gameContext'
import GameReducer from './gameReducer'
import {
    SET_GAME,
    SET_COUNTDOWN

} from '../types'

export const GameState = props => {
    let squares = [];
    for (let i = 0; i < 20; i++) {
        squares[i] = [];
        for (let j = 0; j < 20; j++) {
            squares[i][j] = '';
        }            
    }

    const initialState = {
        game: squares,
        countdown: false
    }

    const [state, dispatch] = useReducer(GameReducer, initialState)

    const getGame = () => {
        
    }

    const setGame = game => {
        dispatch({
            type: SET_GAME,
            payload: game 
        })
    }

    const setCountdown = countdown => {
        dispatch({
            type: SET_COUNTDOWN,
            payload: countdown 
        })
    }


    return (
        <GameContext.Provider
            value={{
                game: state.game,
                scores: state.scores,
                timer: state.timer,
                countdown: state.countdown,
                setCountdown,
                getGame,
                setGame
            }}
        >
            {props.children}
        </GameContext.Provider>
    )
}

export default GameState
