import { GET_GAME, SET_GAME, GET_PLAYERS, SET_PLAYERS, SET_COUNTDOWN } from '../types'

export default (state, action) => {
    switch (action.type) {
        case GET_GAME:
            return [...state, action.payload]
            break;

        case SET_GAME:
            return {
                ...state,
                game: action.payload
            }
            break;
    
        case SET_PLAYERS:
            return {
                ...state,
                game: action.payload
            }
            break;
    
        case GET_PLAYERS:
            return {
                ...state,
                game: action.payload
            }
            break;
    
        case SET_COUNTDOWN:
            return {
                ...state,
                countdown: action.payload
            }
            break;
    
        default:
            return state
    }
}
