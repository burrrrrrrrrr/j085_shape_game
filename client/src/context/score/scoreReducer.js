import { SET_TIMER, SET_SCORES, SET_GAMEOVER } from '../types'

export default (state, action) => {
    switch (action.type) {
        case SET_SCORES:
            return {
                ...state,
                scores: action.payload
            }
            break;
    
        case SET_GAMEOVER:
            return {
                ...state,
                gameOver: action.payload
            }
            break;
    
        case SET_TIMER:
            return {
                ...state,
                timer: action.payload
            }
            break;
    
        default:
            return state
    }
}
