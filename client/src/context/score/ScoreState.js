import React, { useReducer } from 'react'
import ScoreContext from './scoreContext'
import ScoreReducer from './scoreReducer'
import {
    SET_SCORES,
    SET_TIMER,
    SET_GAMEOVER
} from '../types'

export const ScoreState = props => {
    let squares = [];
    for (let i = 0; i < 20; i++) {
        squares[i] = [];
        for (let j = 0; j < 20; j++) {
            squares[i][j] = '';
        }
    }

    const initialState = {
        timer: 0,
        gameOver: false,
        scores: []
    }

    const [state, dispatch] = useReducer(ScoreReducer, initialState)

    const setScores = time => {
        dispatch({
            type: SET_SCORES,
            payload: time 
        })
    }

    const setGameOver = gameOver => {
        dispatch({
            type: SET_GAMEOVER,
            payload: gameOver 
        })
    }

    const setTimer = time => {
        dispatch({
            type: SET_TIMER,
            payload: time 
        })
    }

    return (
        <ScoreContext.Provider
            value={{
                scores: state.scores,
                timer: state.timer,
                gameOver: state.gameOver,
                setGameOver,
                setScores,
                setTimer
            }}
        >
            {props.children}
        </ScoreContext.Provider>
    )
}

export default ScoreState
