export const GET_GAME = 'GET_GAME';
export const SET_GAME = 'SET_GAME';
export const GET_PLAYERS = 'GET_PLAYERS';
export const SET_PLAYERS = 'SET_PLAYERS';
export const SET_SCORES = 'SET_SCORES';
export const SET_TIMER = 'SET_TIMER';
export const SET_GAMEOVER = 'SET_GAMEOVER';
export const SET_COUNTDOWN = 'SET_COUNTDOWN';

