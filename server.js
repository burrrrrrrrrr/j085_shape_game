const express = require('express')
let app = require('express')();
let http = require('http').createServer(app);
let io = require('socket.io')(http);
var path = require('path');
const uuidv1 = require('uuid/v1');

// server assets in products
if(process.env.NODE_ENV === 'production') {
  // set static folder
  app.use(express.static('client/build'))

  app.get('*', (req, res) => res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html')))
} else {
  app.get('/', function(req, res){
    res.sendFile(__dirname + '/client/public/index.html');
  });
}

const port = process.env.PORT || 5000

http.listen(port, function(){
  console.log('listening on *:' + port);
});

let players = []

const config = {
  gameTimer: 20000
}

let squares = [];

let playerCount = 0;

let gameTimer = 0;

let gameLoop = {}

const colors = ["#801A00", "#FF0000", "#FF7519", "#FFFF00", "#008A00", "#0033CC", "#660099", "#FF80B5", "#A37ACC", "#FF00FF", "#000000", "#FFFFFF"];

const createPlayerArray = serverPlayers => {
  let clientPlayers = []
  for(const id in serverPlayers) {
    const pl = serverPlayers[id]
    clientPlayers.push({color: pl.color, score: pl.score})
  };
  return clientPlayers
}

const restartGame = () => {
  for (let i = 0; i < 20; i++) {
      squares[i] = [];
      for (let j = 0; j < 20; j++) {
          squares[i][j] = '';
      }            
  }
  for(const id in players) {
    players[id].score = 0;
  };
  gameTimer = 0
}

restartGame()

io.on('connection', function(socket){
  console.log('a user connected: ' + socket.id);
  socket.join('game')
  socket.emit('setSquareClient', {squares: squares});
  players[socket.id] = {}
  players[socket.id].color = colors[playerCount]
  players[socket.id].score = 0
  players[socket.id].countdown = true
  players[socket.id].clientId = uuidv1();
  playerCount++

  socket.on('setSquare', function(square) {
    let canCapture = false;
    square.row = parseInt(square.row)
    square.col = parseInt(square.col)
    const pl = players[socket.id]
    if(pl.score === 0) {
      canCapture = true;
    }
    if(squares[square.row - 1] && squares[square.row - 1][square.col] == pl.color) {
      canCapture = true;
    }
    if(squares[square.row + 1] && squares[square.row + 1][square.col] == pl.color) {
      canCapture = true;
    }
    if(squares[square.row][square.col - 1] && squares[square.row][square.col - 1] == pl.color) {
      canCapture = true;
    }
    if(squares[square.row][square.col + 1] && squares[square.row][square.col + 1] == pl.color) {
      canCapture = true;
    }
    if(squares[square.row][square.col] !== "") {
      canCapture = false
    }
    if(gameTimer > config.gameTimer) {
      canCapture = false
    }

    if (canCapture && pl.countdown) {
      ++pl.score
      squares[square.row][square.col] = pl.color
      io.in('game').emit('setSquareClient', {squares: squares, didCapture: true})
      io.to(socket.id).emit('setCountdown', false)
      pl.countdown = false

      setTimeout(() => {
        io.to(socket.id).emit('setCountdown', true)
        pl.countdown = true
      }, 1000)
    }
  });

  // socket.on('registerPlayer', function(player){
  //   socket.emit('registerPlayer', {name: players[socket.id], color: players[socket.id].color});
  // });

  if(playerCount === 1) {
    gameLoop = () => {
      const pls = createPlayerArray(players)
      io.in('game').emit('gameOver', {gameOver: false, scores: pls})
      gameTimer = 0
      
      const gameInterval = setInterval(() => {
        const pls = createPlayerArray(players)
        gameTimer += 1000

        if(gameTimer < config.gameTimer) {
          io.in('game').emit('setScoreTable', {gameTimer: config.gameTimer - gameTimer})
        } else {
          io.in('game').emit('gameOver', {gameOver: true, scores: pls})
          clearInterval(gameInterval)
          
          setTimeout(() => {
            restartGame()
            io.in('game').emit('setSquareClient', {squares: squares})
            gameLoop()
          }, 5000);
        }
      }, 1000);
    }
    
    gameLoop()
  }
});